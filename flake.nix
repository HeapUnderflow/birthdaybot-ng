{
  description = "Rust Development Overlay";

  inputs = {
    nixpkgs.url      = "github:nixos/nixpkgs/nixpkgs-unstable";
    naersk = { url = "github:nix-community/naersk"; inputs.nixpkgs.follows = "nixpkgs"; };
    flake-utils.url  = "github:numtide/flake-utils";

    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, fenix, flake-utils, naersk, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ fenix.overlay ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };

        toolchain = (pkgs.fenix.toolchainOf {
          channel = "stable";
          sha256 = "sha256-8len3i8oTwJSOJZMosGGXHBL5BVuGQnWOT2St5YAUFU=";
        }).withComponents [
          "cargo" "rustc" "clippy" "rustfmt" "rust-src"
        ];

        naersk-lib = naersk.lib."${system}".override {
          rustc = toolchain;
          cargo = toolchain;
        };

        buildInputs = with pkgs; [
          openssl
          pkg-config
          exa
          fd
          toolchain
          valgrind
          massif-visualizer

          clang_14
          mold
        ];

        nativeBuildInputs = with pkgs; [ autoPatchelfHook wrapGAppsHook makeWrapper ];
        ldLibPath = pkgs.lib.makeLibraryPath buildInputs;

        cargo_linker = "${pkgs.clang_14}/bin/clang";
        cargo_rustflags = "-C link-arg=--ld-path=${pkgs.mold}/bin/mold";


      in
        rec {
          packages = {
            birthdaybot-ng = naersk-lib.buildPackage {
              pname = "deframe";
              root = ./.;
              inherit buildInputs nativeBuildInputs;
              postInstall = ''
              wrapProgram $out/bin/birthdaybot_ng --prefix LD_IBRARY_PATH : ${ldLibPath}
              '';
              
              CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_LINKER = cargo_linker;
              CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_RUSTFLAGS = cargo_rustflags;
            };
          };
          
          defaultPackage = packages.birthdaybot-ng;
          
          apps = {
            birthdaybot-ng = flake-utils.lib.mkApp {
              drv = packages.birthdaybot-ng;
            };
          };
          defaultApp = apps.birthdaybot-ng;
        
          devShell = pkgs.mkShell {
            buildInputs = buildInputs ++ [ pkgs.rust-analyzer-nightly ];
            nativeBuildInputs = nativeBuildInputs;

            shellHook = ''
            export CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_LINKER="${cargo_linker}"
            export CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_RUSTFLAGS="${cargo_rustflags}"
            export LD_LIBRARY_PATH="${pkgs.lib.makeLibraryPath buildInputs}:$LD_LIBRARY_PATH"
            '';
          };
        }
    );
}
