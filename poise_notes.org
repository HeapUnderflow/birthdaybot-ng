#+title: Notes on the Poise Framework
#+author: HeapUnderflow

* Pain Points
** Cosmetic

- The ~.user_data_setup~ argument feels very unwieldy
- Grouping args into one macro together breaks?
#+begin_src rust
// this works
#[description = "fetch the next n birthdays (defaults to 5)"]
#[min = 1]
#[max = 15]
n: Option<u8>,

// this doesnt
#[
    description = "fetch the next n birthdays (defaults to 5)",
    min = 1,
    max = 15
]
n: Option<u8>,
#+end_src

** Minor

- poise::command::guild_cooldown -> what duration? seconds? minutes? milliseconds? what format? int literal? string with suffix?
  -> literal number = seconds

** Major
* Huh?'s

- Why is the framework pre-wrapped in a Arc from build?

* Pleasant Suprises

- The ~builtins~ are pretty neat.
