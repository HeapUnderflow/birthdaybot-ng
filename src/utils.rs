use chrono::DateTime;
use chrono::FixedOffset;
use chrono::NaiveDateTime;
use chrono::Utc;

pub fn hammer_absolute(dt: DateTime<FixedOffset>) -> String {
	hammer(dt.timestamp(), 'd')
}

pub fn hammer_relative(dt: DateTime<FixedOffset>) -> String {
	hammer(dt.timestamp(), 'R')
}

fn hammer(stamp: i64, sigil: char) -> String {
	format!("<t:{stamp}:{sigil}>")
}

pub fn make_dt(ndt: NaiveDateTime, offset: i32) -> miette::Result<DateTime<FixedOffset>> {
	Ok(ndt
		.and_local_timezone(Utc)
		.latest()
		.ok_or_else(|| miette::miette!("invalid time for timezone UTC"))?
		.with_timezone(&FixedOffset::east(offset)))
}
