#[macro_use]
extern crate tracing;

use std::sync::Arc;

use miette::IntoDiagnostic;
use migration::MigratorTrait;
use sea_orm::Database;
use tracing_subscriber::EnvFilter;

mod bot;
mod utils;

fn main() -> miette::Result<()> {
	dotenvy::dotenv().into_diagnostic()?;

	tracing_subscriber::fmt()
		.with_env_filter(EnvFilter::from_env("BIRTHDAYBOTNG_LOG"))
		.with_thread_names(true)
		.init();

	let rt = tokio::runtime::Builder::new_multi_thread()
		.enable_all()
		.thread_name(concat!(env!("CARGO_PKG_NAME"), "-wrk"))
		.build()
		.into_diagnostic()?;

	rt.block_on(program())
}

async fn program() -> miette::Result<()> {
	let dburl = std::env::var("DATABASE_URL")
		.map_err(|v| miette::miette!("missing `DATABASE_URL` in environment: {:?}", v))?;
	let token = std::env::var("DISCORD_TOKEN")
		.map_err(|v| miette::miette!("missing `DISCORD_TOKEN` in environment: {:?}", v))?;

	let pool = Arc::new(Database::connect(dburl).await.into_diagnostic()?);

	#[cfg(not(feature = "no-apply-migration"))]
	{
		warn!("applying all pending migrations");
		migration::Migrator::up(&pool, None).await.into_diagnostic()?;
	}

	let bot = bot::Bot::make(token, Arc::clone(&pool)).await?;
	bot.run().await?;

	Ok(())
}
