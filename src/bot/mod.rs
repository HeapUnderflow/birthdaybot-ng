use miette::IntoDiagnostic;
use poise::serenity_prelude as serenity;
use sea_orm::DatabaseConnection;
use std::sync::Arc;

mod commands;

// typedefs used in commands
pub(self) type Error = miette::Error;
pub(self) type ComResult = Result<(), Error>;
pub(self) type Context<'ctx> = poise::Context<'ctx, Data, Error>;
pub(self) type ApplicationContext<'ctx> = poise::ApplicationContext<'ctx, Data, Error>;

pub struct Data {
	pub db: Arc<DatabaseConnection>,
}

pub struct Bot {
	framework: Arc<poise::Framework<Data, Error>>,
}

impl Bot {
	pub async fn make(token: String, db: Arc<DatabaseConnection>) -> miette::Result<Self> {
		let data = Data { db };

		let framework = poise::Framework::builder()
			.options(poise::FrameworkOptions {
				commands: vec![
					register(),
					help(),
					commands::upcoming(),
					commands::birthday(),
					commands::setup_guild(),
				],
				..Default::default()
			})
			.token(token)
			.intents(serenity::GatewayIntents::non_privileged())
			.user_data_setup(move |_, _, _| Box::pin(async move { Ok(data) }))
			.build()
			.await
			.into_diagnostic()?;

		Ok(Self { framework })
	}

	pub fn get_ctx(&self) -> Arc<serenity::CacheAndHttp> {
		Arc::clone(&self.framework.client().cache_and_http)
	}

	pub async fn run(self) -> miette::Result<()> {
		self.framework.start().await.into_diagnostic()
	}
}

#[poise::command(slash_command, prefix_command)]
async fn help(ctx: Context<'_>, command: Option<String>) -> ComResult {
	let cfg = poise::builtins::HelpConfiguration { ephemeral: true, ..Default::default() };
	poise::builtins::help(ctx, command.as_deref(), cfg).await.into_diagnostic()
}

#[poise::command(prefix_command)]
async fn register(ctx: Context<'_>) -> ComResult {
	poise::builtins::register_application_commands_buttons(ctx).await.into_diagnostic()?;
	Ok(())
}
