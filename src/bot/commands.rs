use chrono::Utc;
use futures::{Stream, StreamExt};
use miette::IntoDiagnostic;
use poise::{
	serenity_prelude::{ChannelId, RoleId},
	Modal,
};

use super::ComResult;
use chrono::FixedOffset;
use entity::birthdays::{self, Entity as Birthdays};
use entity::guilds::{self, Entity as Guilds};
use sea_orm::{prelude::*, QueryOrder, QuerySelect};
use std::sync::Arc;

/// Show the next few upcoming birthdays
#[poise::command(slash_command, prefix_command, guild_cooldown = 60)]
pub async fn upcoming(
	ctx: super::Context<'_>,
	#[description = "fetch the next n birthdays (defaults to 5)"]
	#[min = 1]
	#[max = 15]
	n: Option<u8>,
	#[description = "if false use realnames instead of nicknames"] use_nicks: Option<bool>,
) -> ComResult {
	ctx.defer().await.into_diagnostic()?;

	let db = Arc::clone(&ctx.data().db);
	// limits ]1..15[ D=5
	let n_bd = n.unwrap_or(5).max(1).min(15);
	let now = Utc::now().naive_utc();
	let use_nicks = use_nicks.unwrap_or(true);
	let guild_id = ctx.guild_id().ok_or_else(|| miette::miette!("expected to be in a guild."))?;
	let mut resolved = Vec::new();

	let data = Birthdays::find()
		.filter(birthdays::Column::Birthday.gt(now))
		.order_by_asc(birthdays::Column::Birthday)
		.limit(n_bd as u64)
		.all(&*db)
		.await
		.into_diagnostic()?;

	for bday in data {
		let member_dt = crate::utils::make_dt(bday.birthday, bday.time_zone)?;
		let member = match guild_id.member(ctx.discord(), bday.user_id as u64).await {
			Err(why) => {
				warn!(guild.id=%guild_id.0, user.id=%bday.user_id, ?why, "failed to resolve user");
				(bday.user_id.to_string(), member_dt)
			},
			Ok(member) => {
				if use_nicks {
					(member.display_name().to_string(), member_dt)
				} else {
					(member.user.name.clone(), member_dt)
				}
			},
		};

		resolved.push(member);
	}

	let mut rtext = String::new();

	if resolved.is_empty() {
		rtext.push_str("No Birthdays Registered");
	} else {
		for (name, date) in resolved {
			let abs = crate::utils::hammer_absolute(date);
			let rel = crate::utils::hammer_relative(date);
			let sanitized = name.replace("\n", "").replace("`", "");

			rtext.push_str(&format!("{abs} ({rel}): {sanitized}"));
		}
	}

	poise::send_reply(ctx, |v| {
		v.embed(|e| e.title(format!("Upcoming {} birthdays \u{1F389}", n_bd)).description(rtext))
	})
	.await
	.into_diagnostic()?;

	Ok(())
}

/// Set / Edit your own birthday
#[poise::command(slash_command)]
pub async fn birthday(
	ctx: super::ApplicationContext<'_>,
	#[description = "Set to true to remove birthday"] remove: Option<bool>,
	#[description = "target user id. can only be used by admins"] user: Option<u64>,
) -> ComResult {
	// TODO: Implement "user" option. Ignore it for now
	// -- ApplicationContext feels sketchy
	let db = Arc::clone(&ctx.data.db);
	let user = ctx.interaction.user().id;
	let guild =
		ctx.interaction.guild_id().ok_or_else(|| miette::miette!("expected to be in a guild"))?;

	// lets hope this doesnt break a interaction
	let bd = Birthdays::find()
		.filter(birthdays::Column::GuildId.eq(guild.0).and(birthdays::Column::UserId.eq(user.0)))
		.limit(1)
		.one(&*db)
		.await
		.into_diagnostic()?;

	let seed = match bd {
		Some(v) => {
			if remove.unwrap_or(false) {
				v.delete(&*db).await.into_diagnostic()?;
				poise::send_application_reply(ctx, |b| {
					b.ephemeral(true).content(
						"Successfully removed your birthday from the database for this guild.",
					)
				})
				.await
				.into_diagnostic()?;
				return Ok(());
			}

			BirthdayModal {
				birthday: crate::utils::make_dt(v.birthday, v.time_zone)?
					.format("%Y-%m-%d")
					.to_string(),
				timezone: (v.time_zone / 3600).to_string(),
			}
		},
		None => BirthdayModal { birthday: String::new(), timezone: String::from("0") },
	};

	let response = BirthdayModal::execute_with_defaults(ctx, seed).await.into_diagnostic()?;
	let tz_off = response.timezone.parse::<i32>().into_diagnostic()?.min(12).max(-12);

	let time = chrono::NaiveDate::parse_from_str(&response.birthday, "%Y-%m-%d")
		.into_diagnostic()?
		.and_hms(0, 1, 0)
		.and_local_timezone(FixedOffset::east(tz_off * 3600))
		.latest()
		.ok_or_else(|| miette::miette!("invalid time"))?;

	// TODO parse
	let birthday = time.naive_utc();
	let time_zone = tz_off * 3600;

	Birthdays::insert(birthdays::ActiveModel {
		guild_id: sea_orm::ActiveValue::Set(guild.0 as i64),
		user_id: sea_orm::ActiveValue::Set(user.0 as i64),
		birthday: sea_orm::ActiveValue::Set(birthday),
		time_zone: sea_orm::ActiveValue::Set(time_zone),
	})
	.on_conflict(
		sea_orm::sea_query::OnConflict::columns([
			birthdays::Column::GuildId,
			birthdays::Column::UserId,
		])
		.update_columns([birthdays::Column::Birthday, birthdays::Column::TimeZone])
		.to_owned(),
	)
	.exec(&*db)
	.await
	.into_diagnostic()?;

	poise::send_application_reply(ctx, |v| {
		v.ephemeral(true).content(format!(
			"Added your birthday for the date {} with timezone offset {}",
			time.format("%Y-%m-%d"),
			tz_off
		))
	})
	.await
	.into_diagnostic()?;

	Ok(())
}

#[derive(Debug, poise::Modal)]
#[name = "Create / Edit your Birthday"]
struct BirthdayModal {
	#[name = "Your Birthday (format: yyyy-mm-dd)"]
	#[placeholder = "2022-01-01"]
	#[min_length = 10]
	#[max_length = 10]
	pub birthday: String,

	#[name = "Your TimeZone (offset, ex: -4)"]
	#[placeholder = "-4"]
	#[min_length = 1]
	#[max_length = 4]
	pub timezone: String,
}

/// Force Setup the current guilds in the database
#[poise::command(prefix_command, slash_command, required_permissions = "ADMINISTRATOR")]
pub async fn setup_guild(
	ctx: super::Context<'_>,
	announcement_channel: ChannelId,
	announcement_text: String,
	grant_role: Option<RoleId>,
	remove_role: Option<bool>,
) -> super::ComResult {
	let db = Arc::clone(&ctx.data().db);
	let id = ctx.guild_id().ok_or_else(|| miette::miette!("expected to be in a guild"))?;
	let model = guilds::ActiveModel {
		id: sea_orm::ActiveValue::Set(id.0 as i64),
		announce_in: sea_orm::ActiveValue::Set(Some(announcement_channel.0 as i64)),
		anncounce_format: sea_orm::ActiveValue::Set(announcement_text),
		set_role: sea_orm::ActiveValue::Set(grant_role.map(|v| v.0 as i64)),
		remove_role: sea_orm::ActiveValue::Set(remove_role.unwrap_or(true)),
	};

	Guilds::insert(model)
		.on_conflict(
			sea_orm::sea_query::OnConflict::column(guilds::Column::Id)
				.update_columns([
					guilds::Column::AnnounceIn,
					guilds::Column::AnncounceFormat,
					guilds::Column::SetRole,
					guilds::Column::RemoveRole,
				])
				.to_owned(),
		)
		.exec(&*db)
		.await
		.into_diagnostic()?;

	poise::send_reply(ctx, |v| v.ephemeral(true).content("setup complete"))
		.await
		.into_diagnostic()?;
	Ok(())
}
