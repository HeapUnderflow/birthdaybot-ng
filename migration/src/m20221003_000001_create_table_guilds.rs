use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
	async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
		manager
			.create_table(
				Table::create()
					.table(Guilds::Table)
					.if_not_exists()
					.col(ColumnDef::new(Guilds::Id).big_unsigned().not_null().primary_key())
					.col(ColumnDef::new(Guilds::AnnounceIn).big_unsigned().null())
					.col(ColumnDef::new(Guilds::AnncounceFormat).text().not_null().default(""))
					.col(ColumnDef::new(Guilds::SetRole).big_unsigned().null())
					.col(ColumnDef::new(Guilds::RemoveRole).boolean().not_null().default(false))
					.to_owned(),
			)
			.await
	}

	async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
		manager.drop_table(Table::drop().table(Guilds::Table).to_owned()).await
	}
}

/// Learn more at https://docs.rs/sea-query#iden
#[derive(Iden)]
pub(crate) enum Guilds {
	Table,
	Id,
	AnnounceIn,
	AnncounceFormat,
	SetRole,
	RemoveRole,
}
