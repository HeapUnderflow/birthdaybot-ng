use super::m20221003_000001_create_table_guilds::Guilds;
use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
	async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
		manager
			.create_table(
				Table::create()
					.table(Birthdays::Table)
					.if_not_exists()
					.col(ColumnDef::new(Birthdays::UserId).big_unsigned().not_null())
					.col(ColumnDef::new(Birthdays::GuildId).big_unsigned().not_null())
					.col(ColumnDef::new(Birthdays::Birthday).timestamp().not_null())
					.col(ColumnDef::new(Birthdays::TimeZone).integer().not_null().default(0))
					.primary_key(
						Index::create().col(Birthdays::UserId).col(Birthdays::GuildId).primary(),
					)
					.foreign_key(
						ForeignKey::create()
							.from(Birthdays::Table, Birthdays::GuildId)
							.to(Guilds::Table, Guilds::Id)
							.on_delete(ForeignKeyAction::Cascade),
					)
					.to_owned(),
			)
			.await
	}

	async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
		manager.drop_table(Table::drop().table(Birthdays::Table).to_owned()).await
	}
}

/// Learn more at https://docs.rs/sea-query#iden
#[derive(Iden)]
pub(crate) enum Birthdays {
	Table,
	UserId,
	GuildId,
	Birthday,
	TimeZone,
}
