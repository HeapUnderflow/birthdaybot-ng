pub use sea_orm_migration::prelude::*;

mod m20221003_000001_create_table_guilds;
mod m20221003_000002_create_table_birthdays;
mod m20221003_214927_create_table_awarded_roles;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
	fn migrations() -> Vec<Box<dyn MigrationTrait>> {
		vec![
			Box::new(m20221003_000001_create_table_guilds::Migration),
			Box::new(m20221003_000002_create_table_birthdays::Migration),
			Box::new(m20221003_214927_create_table_awarded_roles::Migration),
		]
	}
}
