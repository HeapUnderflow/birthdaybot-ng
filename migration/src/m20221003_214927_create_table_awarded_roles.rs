use sea_orm_migration::prelude::*;

use crate::m20221003_000001_create_table_guilds::Guilds;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
	async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
		manager
			.create_table(
				Table::create()
					.table(AwardedRoles::Table)
					.if_not_exists()
					.col(ColumnDef::new(AwardedRoles::UserId).big_unsigned().not_null())
					.col(ColumnDef::new(AwardedRoles::GuildId).big_unsigned().not_null())
					.col(ColumnDef::new(AwardedRoles::RoleId).big_unsigned().not_null())
					.col(ColumnDef::new(AwardedRoles::RemoveAt).timestamp().not_null())
					.primary_key(
						Index::create()
							.col(AwardedRoles::UserId)
							.col(AwardedRoles::GuildId)
							.primary(),
					)
					.foreign_key(
						ForeignKey::create()
							.from(AwardedRoles::Table, AwardedRoles::GuildId)
							.to(Guilds::Table, Guilds::Id)
							.on_delete(ForeignKeyAction::Cascade),
					)
					.to_owned(),
			)
			.await
	}

	async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
		manager.drop_table(Table::drop().table(AwardedRoles::Table).to_owned()).await
	}
}

/// Learn more at https://docs.rs/sea-query#iden
#[derive(Iden)]
enum AwardedRoles {
	Table,
	UserId,
	GuildId,
	RoleId,
	RemoveAt,
}
